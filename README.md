REQUIREMENTS

-------------

Make sure your development environment is ready for the capstone project
1. Visual Studio 2019
2. .NET Framework 8.0


INSTALLATION

------------

Download the above folder and open the solution directly



DETAILS

--------------------------
Project Builder: Kwan-Ming,Tang


APP DESCRIPTION

---------------
This comma sprinkler was built by C#. Reason to build this application is, the English rules for comma
placement are complex, frustrating, and often ambiguous. Many people, even the English, will ignore them,
and apply custom rules, or no rules at all.

In order to solve this issue, the application would automatically insert commas among the text.


USER INSTRUCTIONS

-----------------

Please run this application on Visual Studio. Also, you may need to create a txt file and 
input a phrase into the txt.file and save it. After that, select the txt file when you run 
the application in Visual Studio, and execute it.