﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Comma_Sprinkler
{
    class OutputGenerator
    {
        public string GenerateOutput(string input, List<string> beforeComma, List<string> afterComma)
        {
            StringBuilder stringBuilder = new StringBuilder();
            string[] words = input.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string word in words)
            {
                if (word.EndsWith(",") || word.EndsWith("."))
                {
                    stringBuilder.Append(word + " ");
                }
                else
                {
                    bool handled = false;
                    foreach (string item in afterComma)
                    {
                        if (word.Contains(item))
                        {
                            stringBuilder.Append(word + ", ");
                            handled = true;
                        }
                    }
                    if (!handled)
                    {
                        stringBuilder.Append(word + " ");
                    }
                }
            }
            string priorWord = null;
            words = stringBuilder.ToString().Split(' ');
            stringBuilder.Clear();
            foreach (string word in words)
            {
                bool exists = false;
                foreach (string item in beforeComma)
                {
                    if (item == word.TrimEnd(',', '.'))
                    {
                        exists = true;
                    }
                }
                if (exists)
                {
                    if (priorWord != null && !priorWord.Contains(',') && !priorWord.Contains('.'))
                    {
                        stringBuilder.Append(priorWord + ", ");
                    }
                    else
                    {
                        stringBuilder.Append(priorWord + " ");
                    }
                }
                else
                {
                    if (priorWord != null)
                    {
                        stringBuilder.Append(priorWord + " ");
                    }
                }
                priorWord = word;
            }
            stringBuilder.Append(priorWord);
            return stringBuilder.ToString();
        }
    }
}
