﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Comma_Sprinkler
{
    class CommaBuilder
    {
        public void SprinkleCommas(string input, List<string> beforeComma, List<string> afterComma)
        {
            string[] sentences = input.Split(new[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string sentence in sentences)
            {
                if (sentence.Contains(','))
                {
                    string[] phrases = sentence.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 0; i < phrases.Length; i++)
                    {
                        string phrase = phrases[i];
                        if (i < phrases.Length - 1)
                        {
                            string[] words = phrase.Split(' ');
                            if (!afterComma.Any(p => p == words.Last()))
                                afterComma.Add(words.Last());
                        }
                        if (i > 0)
                        {
                            string[] precedingWith = phrase.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                            if (precedingWith.First() != null && !beforeComma.Any(p => p == precedingWith.First()))
                            {
                                beforeComma.Add(precedingWith.First());
                            }
                        }
                    }
                }
            }
        }
    }
}
