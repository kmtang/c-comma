﻿using System.Windows;

namespace Comma_Sprinkler
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        readonly VM vm = new VM();
        public MainWindow()
        {
            InitializeComponent();
            DataContext = vm;
        }

        private void Select_Click(object sender, RoutedEventArgs e)
        {
            vm.Select();
        }

        private void Change_Click(object sender, RoutedEventArgs e)
        {
            vm.Change();
        }
    }
}
