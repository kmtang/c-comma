﻿using Microsoft.Win32;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Comma_Sprinkler
{
    class VM : INotifyPropertyChanged
    {
        private readonly List<string> beforeComma = new List<string>();
        private readonly List<string> afterComma = new List<string>();
        private readonly CommaBuilder commaBuilder = new CommaBuilder();
        private readonly OutputGenerator outputGenerator = new OutputGenerator();
        private string message = "";
        public string Message
        {
            get { return message; }
            set { message = value; NotifyChanged(); }
        }

        private string output = "";
        public string Output
        {
            get { return output; }
            set { output = value; NotifyChanged(); }
        }

        public void Select()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Please select the txt file";
            openFileDialog.Filter = "Text Files(.txt)|*.txt|All Files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == true)
            {
                string theFile = openFileDialog.FileName;
                Message = System.IO.File.ReadAllText(theFile);
            }
        }

        public void Change()
        {
            Output = Message;
            string result = Output;
            do
            {
                var temp = SprinkleCommas(result);
                if (temp == result)
                {
                    break;
                }
                result = temp;
            } while (true);
            Output = result;
        }

        private string SprinkleCommas(string source)
        {
            beforeComma.Clear();
            afterComma.Clear();
            commaBuilder.SprinkleCommas(source, beforeComma, afterComma);
            var result = outputGenerator.GenerateOutput(source, beforeComma, afterComma);
            return result;
        }
        #region PropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyChanged([CallerMemberName] string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        #endregion
    }
}
